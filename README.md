# FER4Crowd
Il sistema è stato sviluppato e testato su Linux Mint 19. Ne è stata anche verificata la compatibilità su Windows 10.
Il software fa parte di un progetto di tesi, per tanto lo si può considerare come una *beta version*; naturalmente, per entrare in produzione, necessiterà di ulteriori test, ottimizzazioni e un ammodernamento della GUI.

Per una panoramica completa delle funzionalità di FER4Crowd, di come estendere il pool dei classificatori di emozioni e dei rilevatori di visi, si rimanda al capitolo **FER4Crowd** della tesi di laurea, presente nella cartella **doc**.

## # Installazione delle dipendenze
**Versione di Python consigliata:** 3.6.9

Per velocizzare questo step, se non si ha in mente di utilizzare i *face detector* HOG e CNN, è possibile rimuovere da **requirements.txt** (presente nella root del repository) le righe:

    dlib==19.21.0
    face-recognition==1.3.0
    face-recognition-models==0.3.0 

 - *[Nel caso non siano state rimosse tali dipendenze]* Se si sta operando su Linux (probabilmente lo stesso vale per MacOS) sarà necessario prima installare **cmake** :\
`$ sudo apt-get install cmake`

### Creazione e attivazione del Virtual Environment di Python
Gli step seguenti valgono per Linux. Per gli altri sistemi si rimanda alle guide ufficiali o al seguente tutorial: https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/
 - Creare un Virtual Environment di Python nella root del progetto, digitando nel terminale:\
 `$ python3 -m venv venv`

 - Attivare il Virtual Environment:\
`$ source venv/bin/activate`

- Aggiornare la versione dell'utility **pip**:\
`(venv)$ pip install --upgrade pip`

### Installazione delle librerie
Nella root del progetto eseguire:\
`(venv)$ pip install -r requirements.txt`

## # Gestione delle API

A questo punto eseguendo **main.py** \
`(venv)$ python3 fer4crowd/main.py` \
si avvierà la GUI di FER4Crowd, ma non sarà ancora possibile generare un report in Google Drive o utilizzare le FaceAPI. È necessario indicare le credenziali per l'utilizzo di questi servizi come segue.

### Google API
Seguendo le indicazioni nella documentazione della libreria **gspread**, è possibile attivare le API di Google Drive e Google Sheets e registrare un Service Account.
Le sezioni di interesse della guida sono **[Enable API Access for a Project](https://gspread.readthedocs.io/en/latest/oauth2.html#enable-api-access-for-a-project)** e **[For Bots: Using Service Account](https://gspread.readthedocs.io/en/latest/oauth2.html#for-bots-using-service-account)** (fino allo step 5).

Scaricato  il file JSON delle credenziali, rinominarlo in "google_api_credentials.json" e spostarlo nella cartella **fer4crowd/config** del progetto (sovrascrivere il file già presente).

In Google Drive sarà necessario fornire i permessi di scrittura, all'email in corrispondenza del campo *client_email* nel file JSON, per le cartelle in cui si vogliono generare i report o i moduli di annotazione.

### Microsoft Face API
Per utilizzare le Face API per il riconoscimento facciale:

 - Registrare un account su https://azure.microsoft.com/it-it/services/cognitive-services/face/ (l'opzione gratuita è molto limitata, è  facile raggiungere il limite di utilizzo)
 
 - Riportare *endpoint* e *key* nel file **fer4crowd/emotion_classifiers/face_api/config.ini**
 - De-commentare la linea relativa alle FaceAPI nel file  **fer4crowd/emotion_classifiers/classifiers.py**

