from pathlib import Path
from cv2 import CascadeClassifier
from fer4crowd.face_detectors.BaseDetector import BaseDetector


class Haar(BaseDetector):

    @staticmethod
    def detect_faces(bgr_frame, **kwargs):
        detector = CascadeClassifier(str(Path(__file__).parent/"haarcascade_frontalface_default.xml"))
        return detector.detectMultiScale(bgr_frame, **kwargs)