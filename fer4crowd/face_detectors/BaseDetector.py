class BaseDetector:

    @staticmethod
    def coordinates2rect(coordinates):
        """
        :param coordinates: (top, right, bottom, left)
        :return: x, y, width, height
        """
        top, right, bottom, left = coordinates
        return left, top, right-left, bottom-top

    @staticmethod
    def detect_faces(bgr_frame, **kwargs):
        """
        :param bgr_frame:
        :return: Face rectangles: list of (x, y, width, height)
        """
        raise NotImplementedError()

