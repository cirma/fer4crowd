from face_recognition import face_locations
from fer4crowd.face_detectors.BaseDetector import BaseDetector


class Hog(BaseDetector):

    @staticmethod
    def detect_faces(bgr_frame, **kwargs):
        rectangles = []
        for coordinates in face_locations(bgr_frame, model="hog", **kwargs):
            rectangles.append(BaseDetector.coordinates2rect(coordinates))
        return rectangles



