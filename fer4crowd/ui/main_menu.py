import PySimpleGUI as sg

from fer4crowd.ui.evaluation_views import EvaluationMenu
from fer4crowd.ui.video_analysis_views import AnalysisForm
from fer4crowd.ui.view import View


class MainMenu(View):
    def _generate_layout(self):
        buttons = [[sg.Button(key="BTN_evaluate", button_text='Evaluate FER systems')],
                   [sg.Button(key="BTN_analyze", button_text='Analyze a video')],
                   [sg.Button('Exit')]]
        return [[sg.Column(buttons, element_justification="center", pad=(4, 4))]]

    def _hook_after_window_generation(self):
        self.window["BTN_evaluate"].expand(expand_x=True)
        self.window["BTN_analyze"].expand(expand_x=True)

    def _event_handler(self, event, values):
        if event == sg.WIN_CLOSED or event == 'Exit':
            self.send_close_signal()
        elif event == "BTN_evaluate":
            evaluation_menu = EvaluationMenu(self.gui)
            self._switch_to_view(evaluation_menu, keep_current_view=True)
        elif event == "BTN_analyze":
            analysis_form = AnalysisForm(self.gui)
            self._switch_to_view(analysis_form, keep_current_view=True)
