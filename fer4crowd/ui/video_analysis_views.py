import queue
import sys
import threading
import traceback
import webbrowser
from pathlib import Path
from queue import Queue

import PySimpleGUI as sg
import cv2

from fer4crowd.emotion_classifiers.classifiers import get_available_classifiers
from fer4crowd.report.gspreadsheet import GoogleSheet
from fer4crowd.ui.view import View, LOG_FILE
from fer4crowd.video_analysis import VideoAnalyzer


class AnalysisForm(View):
    __OFFLINE_REPORT = "Offline report"
    __GOOGLE_DRIVE_REPORT = "Google Drive report"

    @staticmethod
    def _get_classifiers_combobox(key):
        classifiers = get_available_classifiers()
        choices = [c_name for c_name in list(classifiers.keys())]
        return sg.InputCombo(k=key, values=choices, default_value=choices[0], readonly=True, enable_events=True)

    def _generate_layout(self):
        cache = self._get_cache()
        default_video_path = cache.get("default_video_path", "")
        default_report_folder = cache.get("default_report_folder", "")

        layout = [
            # TITLE
            [sg.Text(k="Title", text="Video analysis", justification="center", font=("Helvetica", 25))],

            # Open centered layout
            [sg.Column([
                # VIDEO FILE SELECTION
                [sg.Text('Video', font=('Helvetica', 10), justification='left'), sg.Input(k="INPUT_video"),
                 sg.FileBrowse(initial_folder=default_video_path)],

                # REPORT
                [sg.Frame(title="Report", layout=[
                    [sg.Text(text='Report name', size=(13, 1)), sg.In(k="INPUT_report_name", size=(28, 1))],
                    [sg.Text(text='Report type', size=(13, 1)), sg.InputCombo(k="COMBO_report_type",
                                                                              size=(26, 1),
                                                                              values=(
                                                                                  self.__OFFLINE_REPORT,
                                                                                  self.__GOOGLE_DRIVE_REPORT),
                                                                              default_value=self.__OFFLINE_REPORT,
                                                                              readonly=True,
                                                                              enable_events=True)],
                    [sg.pin(sg.Column(k="drive_folder", pad=(0, 0), layout=[
                        [sg.Text(text='Drive folder id', size=(13, 1)),
                         sg.In(k="INPUT_drive_folder_id", size=(28, 1))],
                        [sg.Text(k="h1",
                                 text="You can find it at the end of the folder url. \nE.g: 1m1BsSRKft7n_k3d8pWxZisvrk8rYfLRo5s",
                                 font=('Courier', 8))]
                    ]))],

                    [sg.pin(sg.Column(k="offline_report_folder", pad=(0, 0), layout=[
                        [sg.Text(text='Report folder', size=(13, 1)),
                         sg.In(k="INPUT_offline_report_folder", size=(18, 1)),
                         sg.FolderBrowse(initial_folder=default_report_folder)],

                    ]))],

                    # SHOW MILLISECONDS
                    [sg.Text(k="s1", text="", pad=(0, 0))],
                    [sg.Checkbox(k="CHECKBOX_show_milliseconds", text='Show milliseconds in the report', pad=(0, 0))],
                ])]
            ], justification="center", element_justification="center")],
            # close centerd layout

            # Open row
            # OPTIONS
            [sg.Frame(title="Options", layout=[
                [sg.Checkbox(k="CHECKBOX_time_frame", text='Analyze only a time frame', enable_events=True,
                             pad=(0, 0))],
                [sg.pin(sg.Column(k="time_frame_options", pad=(0, 0), layout=[
                    [sg.Text(text='Start second', size=(13, 1)),
                     sg.In(k="INPUT_start_second", default_text="0", size=(22, 1))],
                    [sg.Text(text='End second', size=(13, 1)),
                     sg.In(k="INPUT_end_second", default_text="120", size=(22, 1))],
                    [sg.Checkbox(k="CHECKBOX_zero_time_labeling", text='Start the time labeling from 00:00',
                                 pad=(0, 0))],

                ]))],

                # FRAME WIDTH
                [sg.Text(k="s2", text="", pad=(0, 0))],
                [sg.Checkbox(k="CHECKBOX_resize_frames", text='Resize frames', enable_events=True, pad=(0, 0))],

                [sg.pin(sg.Column(k="resize_frames_options", pad=(0, 0), layout=[
                    [sg.Text(text='Frame width (pixels)'),
                     sg.In(k="INPUT_frame_width", default_text="500", size=(22, 1))],
                ]))],

                # MILLISECONDS HOP
                [sg.Text("", pad=(0, 0), k="s3")],
                [sg.Text(text='Analyze a frame every'),
                 sg.In(k="INPUT_ms_hop", default_text="1000", size=(5, 1)),
                 sg.Text(text='milliseconds')],
                [sg.Text(text="A very low value can significantly slow down the analysis", font=('Courier', 8))],
            ]),

             # CLASSIFIER
             sg.Frame(k="FRAME_classifier", title="Emotion classifier", layout=[
                 [sg.Text('Classifier'),
                  self._get_classifiers_combobox(key="COMBO_classifier")],

                 [sg.Text("", pad=(0, 0))],
                 [sg.Text('Emotions to ignore')],
                 [sg.Listbox(k="LISTBOX_emos_to_ignore", values=[],
                             size=(33, 4),
                             select_mode=sg.LISTBOX_SELECT_MODE_EXTENDED)],
                 [sg.Text(text="Highlight the emotions to ignore", font=('Courier', 8))]])
             ],
            # close row

            # ACTIONS
            [sg.Text("", pad=(0, 0))],
            [sg.Column([
                [sg.Button(key="BTN_back", button_text="Back", font=("Helvetica", 13)),
                 sg.Button(k="BTN_start", button_text="Start analysis (faster)", font=("Helvetica", 13, "bold")),
                 sg.Button(k="BTN_start_visual", button_text="Start visual analysis", font=("Helvetica", 13, "bold"))],
            ], justification="center", element_justification="center")],

        ]
        return layout

    def _hook_after_window_generation(self):
        self.window["Title"].expand(expand_x=True)
        self.window["COMBO_classifier"].expand(expand_x=True)
        self.window["drive_folder"].update(visible=False)
        self.window["time_frame_options"].update(visible=False)
        self.window["resize_frames_options"].update(visible=False)
        self.window["FRAME_classifier"].expand(expand_x=True)
        self.selected_classifier = get_available_classifiers()[self.window["COMBO_classifier"].get()]()
        self.window["LISTBOX_emos_to_ignore"].update(values=self.selected_classifier.get_emotion_labels())

    def _event_handler(self, event, values):
        if event == 'BTN_back':
            self.send_close_signal()

        elif event == sg.WIN_CLOSED:
            exit()

        elif event == "COMBO_report_type":
            if self.window["COMBO_report_type"].get() == self.__OFFLINE_REPORT:
                self.window["drive_folder"].update(visible=False)
                self.window["offline_report_folder"].update(visible=True)

            elif self.window["COMBO_report_type"].get() == self.__GOOGLE_DRIVE_REPORT:
                self.window["drive_folder"].update(visible=True)
                self.window["offline_report_folder"].update(visible=False)

        elif event == "CHECKBOX_resize_frames":
            self.window["resize_frames_options"].update(visible=bool(self.window["CHECKBOX_resize_frames"].get()))

        elif event == "CHECKBOX_time_frame":
            self.window["time_frame_options"].update(visible=bool(self.window["CHECKBOX_time_frame"].get()))

        elif event == "COMBO_classifier":
            self.selected_classifier = get_available_classifiers()[self.window["COMBO_classifier"].get()]()
            self.window["LISTBOX_emos_to_ignore"].update(values=self.selected_classifier.get_emotion_labels())

        elif event == "BTN_start" or "BTN_start_visual":
            self._store_cache({
                "default_video_path": str(Path(self.window["INPUT_video"].get()).parent),
                "default_report_folder": str(Path(self.window["INPUT_offline_report_folder"].get()))
            })

            visual_analysis = (event == "BTN_start_visual")

            if bool(self.window["CHECKBOX_time_frame"].get()):
                start_second = int(self.window["INPUT_start_second"].get())
                end_second = int(self.window["INPUT_end_second"].get())
            else:
                start_second = end_second = None

            if self.window["COMBO_report_type"].get() == self.__OFFLINE_REPORT:
                report_type = VideoAnalyzer.OFFLINE_REPORT
            else:
                report_type = VideoAnalyzer.GOOGLE_SHEET_REPORT

            if bool(self.window["CHECKBOX_resize_frames"].get()):
                frame_width = int(self.window["INPUT_frame_width"].get())
            else:
                frame_width = -1

            progress_view = AnalysisProgress(self.gui,
                                             emo_classifier=self.selected_classifier,
                                             ms_hop=int(self.window["INPUT_ms_hop"].get()),
                                             frame_width=frame_width,
                                             emos_to_ignore=self.window["LISTBOX_emos_to_ignore"].get(),
                                             video_path=self.window["INPUT_video"].get(),
                                             report_name=self.window["INPUT_report_name"].get(),
                                             visual_analysis=visual_analysis,
                                             time_frame=([start_second, end_second,
                                                          bool(self.window["CHECKBOX_zero_time_labeling"].get())]),
                                             time_offset=0,
                                             show_milliseconds=bool(self.window["CHECKBOX_show_milliseconds"].get()),
                                             report=report_type,
                                             drive_folder_id=self.window["INPUT_drive_folder_id"].get(),
                                             report_folder=self.window["INPUT_offline_report_folder"].get())
            self._switch_to_view(progress_view, keep_current_view=True)


# ANALYSIS PROGRESS VIEW
class AnalysisProgress(View):
    class _AnalysisThread(threading.Thread):
        def run(self):
            exception_callback = self._kwargs["exception_callback"]
            del self._kwargs["exception_callback"]
            try:
                self._target(*self._args, **self._kwargs)
            except Exception as e:
                traceback.print_exc(file=open(LOG_FILE, "w+"))
                exception_callback(str(e))

    def __init__(self, gui, **kwargs):
        super().__init__(gui)
        self.kwargs = kwargs
        self.__analysis_stopped = False

    def _generate_layout(self):
        layout = [[sg.Text(k="TXT_progress", text='Analyzing...', size=(60, 1))],
                  [sg.ProgressBar(100, orientation='h', size=(60, 20), key='progbar')],
                  [sg.pin(sg.Button(k="BTN_report", button_text="Open report folder")),
                   sg.pin(sg.Button(k="BTN_cancel", button_text="Cancel"))]]
        return layout

    def _hook_after_window_generation(self):
        kwargs = self.kwargs
        self.window["BTN_report"].update(visible=False)

        if kwargs.get("visual_analysis"):
            self.window["BTN_cancel"].update(visible=False)  # OpenCV can't manage two consecutive calls to cv2.imshow()

        self.video_analyzer = VideoAnalyzer(emo_classifier=kwargs.get("emo_classifier"),
                                            ms_hop=kwargs.get("ms_hop"),
                                            frame_width=kwargs.get("frame_width"),
                                            emos_to_ignore=kwargs.get("emos_to_ignore"),
                                            progress_callback=self.progress_callback)

        self.thrd = AnalysisProgress._AnalysisThread(target=self.video_analyzer.analyze_video,
                                                     kwargs=dict(
                                                         exception_callback=self.exception_callback,
                                                         video_path=kwargs.get("video_path"),
                                                         report_name=kwargs.get("report_name"),
                                                         visual_analysis=kwargs.get("visual_analysis"),
                                                         time_frame=kwargs.get("time_frame"),
                                                         time_offset=kwargs.get("time_offset"),
                                                         show_milliseconds=kwargs.get("show_milliseconds"),
                                                         report=kwargs.get("report"),
                                                         drive_folder_id=kwargs.get("drive_folder_id"),
                                                         report_folder=kwargs.get("report_folder"),
                                                     ), daemon=True)

        self.thrd.start()

    def progress_callback(self, progress_percentage, next_step_description=None, complete=False):
        if self.__analysis_stopped:
            return

        if complete:
            self.window.write_event_value("COMPLETE", None)
            return

        self.window["progbar"].update_bar(progress_percentage)
        if next_step_description is not None:
            self.window["TXT_progress"].update(value=next_step_description)

    def _event_handler(self, event, values):
        if event == "Exception":
            self.send_close_signal()
            self.window.hide()
            sg.popup(values["Exception"], title="Error", icon=self.gui.icon, location=self.window.current_location())

        elif event == "BTN_cancel":
            self.__analysis_stopped = True
            self.window["TXT_progress"].update(value="Stopping analysis...")
            self.video_analyzer.stop()
            self.thrd.join(30)
            self.__analysis_stopped = False
            self.send_close_signal()

        elif event == sg.WIN_CLOSED:
            exit()

        elif event == "COMPLETE":
            self.window["TXT_progress"].update(value="Complete")
            self.window["progbar"].update_bar(100)
            self.window["BTN_cancel"].update(text="Continue")
            self.window["BTN_report"].update(visible=True)

        elif event == "BTN_report":
            folder = None
            report = self.kwargs.get("report")
            if report == VideoAnalyzer.OFFLINE_REPORT:
                folder = self.kwargs.get("report_folder")
            elif report == VideoAnalyzer.GOOGLE_SHEET_REPORT:
                folder = GoogleSheet.DRIVE_FOLDER_BASE_URL % self.kwargs.get("drive_folder_id")
            webbrowser.get().open(folder)
