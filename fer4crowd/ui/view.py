import _tkinter
import os
import glob
import pickle
from pathlib import Path
import PySimpleGUI as sg

LOG_FILE = str(Path(__file__).parents[1] / "error-log.txt")
CACHE_FOLDER = str(Path(__file__).parent / "cache")
RECOVERY_FOLDER = str(Path(CACHE_FOLDER) / "recovery")

for f in glob.glob(str(Path(RECOVERY_FOLDER) / "*.rec")):
    os.remove(f)  # delete old recovery files


class View:
    def __init__(self, gui):
        """
        :param kwargs: Args of PySimpleGUI.Window
        :return:
        """
        self.gui = gui
        self.__recovery_file = None
        self.__window_cache = dict()
        self.window = None  # type: sg.Window or None
        self.close_signal = False
        self._read_timeout = None

    def _generate_layout(self):
        """
        :return: PySimpleGUI layout
        :rtype: List[List[Elements]]
        """
        raise NotImplementedError()

    def _event_handler(self, event, values):
        if event == sg.WIN_CLOSED:
            self.send_close_signal()
            return

    def _hook_after_window_generation(self):
        """
        Actions to perform immediately after the window generation
        """
        return

    # From here don't override

    def _switch_to_view(self, new_view, keep_current_view=True, **kwargs):
        """
        :type window: sg.Window
        :type new_view: View
        :param keep_current_view: If False this view will be closed.
        :param kwargs: Args of PySimpleGUI.Window for the new view window
        """
        if keep_current_view:
            self.__recovery_file = str(Path(RECOVERY_FOLDER) / (str(id(self)) + ".rec"))
            open(self.__recovery_file, "w+").close()
            self.window.save_to_disk(self.__recovery_file)
            self.__window_cache["location"] = self.window.current_location()
        else:
            self.send_close_signal()

        kwargs.pop("location", "")
        self.window.hide()
        #new_view.show(location=self.window.current_location(), **kwargs)
        new_view.show(**kwargs)
        self.window.un_hide()

    def __generate_window(self, hook=True, **kwargs):
        self.window = sg.Window(title=kwargs.pop("title", self.gui.title),
                                layout=kwargs.pop("layout", self._generate_layout()),
                                icon=kwargs.pop("icon", self.gui.icon),
                                font=kwargs.pop("font", ("Helvetica", 11)),
                                finalize=kwargs.pop("finalize", True),
                                **kwargs)
        if hook:
            self._hook_after_window_generation()
        return self.window

    def _store_cache(self, cache):
        """
        Overwrites the old cache
        :param cache: dict
        """
        cache_file = Path(CACHE_FOLDER) / (self.__class__.__name__ + ".cache")
        pickle.dump(cache, open(cache_file, "wb+"))

    def _get_cache(self):
        """
        :rtype: dict
        """
        cache_file = Path(CACHE_FOLDER) / (self.__class__.__name__ + ".cache")
        try:
            return pickle.load(open(cache_file, "rb"))
        except (OSError, IOError):
            return dict()

    def enable_animations(self):
        self._read_timeout = 25

    def show(self, **kwargs):
        self.__generate_window(**kwargs)

        while True and not self.close_signal:
            try:
                event, values = self.window.read(timeout=self._read_timeout)

                if event == "Exception":
                    self.send_close_signal()
                    self.window.hide()
                    sg.popup(values["Exception"], title="Error", icon=self.gui.icon,
                             location=self.window.current_location())

                self._event_handler(event, values)
            except _tkinter.TclError:   # Tkinter could close windows without notifying
                if self.__recovery_file is not None:
                    kwargs.pop("location", "")
                    self.__generate_window(location=self.__window_cache["location"], hook=False, **kwargs)
                    self.window.load_from_disk(self.__recovery_file)
                    self._hook_after_window_generation()
            finally:
                if self.__recovery_file is not None:
                    self.__recovery_file = None
        self.window.close()
        self.close_signal = False

    def send_close_signal(self):
        self.close_signal = True

    def exception_callback(self, message=None):
        if message is None or message == "":
            message = "An error occurred\nCheck the error log file:" + LOG_FILE
        else:
            message += "\n\nCheck the error log file: " + LOG_FILE
        self.window.write_event_value("Exception", message)

