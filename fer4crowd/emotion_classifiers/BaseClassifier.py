class BaseClassifier:

    def get_emotion_labels(self):
        """
        :return: List of emotion labels
        :rtype: list
        """
        raise NotImplementedError()

    def predict(self, bgr_frame):
        """
        :param bgr_frame: BGR frame
        :return: Emotion predictions: list of tuples (detected_face_rectangle, face_emotions)
        face_rectangle: (x,y,width,height)
        face_emotions: list of emotion predictions. It must follow the same order as the list
        of emotion labels returned by get_emotion
        :rtype: list of (list,list)
        """
        raise NotImplementedError()
