from pathlib import Path
from fer4crowd.emotion_classifiers.BaseClassifier import BaseClassifier
from fer4crowd.face_detectors.haar.Haar import Haar
from keras.models import load_model, model_from_json
import numpy as np
import cv2


class GitShanks(BaseClassifier):

    def __init__(self):
        json_file = open(str(Path(__file__).parent/"fer.json"), 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        self.loaded_model.load_weights(str(Path(__file__).parent/"fer.h5"))

    def get_emotion_labels(self):
        """
        :return: List of emotion labels
        """
        return ["anger", "disgust", "fear", "happiness", "sadness", "surprise", "neutral"]

    def __detect_faces(self, bgr_frame):
        return Haar.detect_faces(bgr_frame)

    def predict(self, bgr_frame):
        """
        :param bgr_frame: BGR frame
        :return: Emotion predictions: list of tuples (detected_face_rectangle, face_emotions)
        face_rectangle: (x,y,width,height)
        face_emotions: list of emotion predictions. It must follow the same order as the list of emotion labels returned by get_emotion
        """

        detected_faces = self.__detect_faces(bgr_frame)
        predictions = []
        gray = cv2.cvtColor(bgr_frame, cv2.COLOR_BGR2GRAY)

        for face_rectangle in detected_faces:
            x, y, w, h = face_rectangle
            roi_gray = gray[y:y + h, x:x + w]  # Region of Interest
            cropped_img = np.expand_dims(np.expand_dims(cv2.resize(roi_gray, (48, 48)), -1), 0)
            cv2.normalize(cropped_img, cropped_img, alpha=0, beta=1, norm_type=cv2.NORM_L2, dtype=cv2.CV_32F)
            # predicting the emotion
            preds = self.loaded_model.predict(cropped_img).tolist()[0]
            predictions.append((list(face_rectangle), preds))

        return predictions
