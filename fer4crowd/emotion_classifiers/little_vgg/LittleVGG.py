import time
from pathlib import Path
from fer4crowd.emotion_classifiers.BaseClassifier import BaseClassifier
from fer4crowd.face_detectors.haar.Haar import Haar
from keras.models import load_model
from keras.preprocessing.image import img_to_array
import numpy as np
import cv2


class LittleVGG(BaseClassifier):

    def __init__(self):
        self.classifier = load_model(str(Path(__file__).parent/"littleVGG.hdf5"))

    def get_emotion_labels(self):
        """
        :return: List of emotion labels
        """
        return ["anger", "disgust", "fear", "happiness", "neutral", "sadness", "surprise"]

    def __detect_faces(self, bgr_frame):
        return Haar.detect_faces(bgr_frame, scaleFactor=1.15)

    def predict(self, bgr_frame):
        """
        :param bgr_frame: BGR frame
        :return: Emotion predictions: list of tuples (detected_face_rectangle, face_emotions)
        face_rectangle: (x,y,width,height)
        face_emotions: list of emotion predictions. It must follow the same order as the list of emotion labels returned by get_emotion
        """

        detected_faces = self.__detect_faces(bgr_frame)
        predictions = []
        gray = cv2.cvtColor(bgr_frame, cv2.COLOR_BGR2GRAY)

        for face_rectangle in detected_faces:
            x, y, w, h = face_rectangle
            roi_gray = gray[y:y + h, x:x + w]  # Region of Interest
            roi_gray = cv2.resize(roi_gray, (48, 48), interpolation=cv2.INTER_AREA)

            roi = roi_gray.astype("float") / 255.0
            roi = img_to_array(roi)
            roi = np.expand_dims(roi, axis=0)

            # make a prediction on the ROI, then lookup the class
            preds = list(self.classifier.predict(roi)[0])
            predictions.append((list(face_rectangle), preds))

        return predictions
