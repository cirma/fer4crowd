from pathlib import Path
from fer4crowd.emotion_classifiers.BaseClassifier import BaseClassifier
from fer4crowd.face_detectors.haar.Haar import Haar
from keras.models import load_model
import numpy as np
import cv2


class MiniXCEPTION(BaseClassifier):

    def __init__(self):
        self.emotion_model_path = str(Path(__file__).parent/"fer2013_mini_XCEPTION.107-0.66.hdf5")

        # hyper-parameters for bounding boxes shape
        self.frame_window = 10
        self.emotion_offsets = (20, 40)

        self.emotion_classifier = load_model(self.emotion_model_path, compile=False)

        # getting input model shapes for inference
        self.emotion_target_size = self.emotion_classifier.input_shape[1:3]

    def get_emotion_labels(self):
        """
        :return: List of emotion labels
        """
        return ["anger", "disgust", "fear", "happiness", "sadness", "surprise", "neutral"]

    def __apply_offsets(self, face_coordinates, offsets):
        x, y, width, height = face_coordinates
        x_off, y_off = offsets
        return x - x_off, x + width + x_off, y - y_off, y + height + y_off

    def __preprocess_input(self, x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    def __detect_faces(self, bgr_frame):
        return Haar.detect_faces(bgr_frame, scaleFactor=1.15)

    def predict(self, bgr_frame):
        """
        :param bgr_frame: BGR frame
        :return: Emotion predictions: list of tuples (detected_face_rectangle, face_emotions)
        face_rectangle: (x,y,width,height)
        face_emotions: list of emotion predictions. It must follow the same order as the list of emotion labels returned by get_emotion
        """

        detected_faces = self.__detect_faces(bgr_frame)
        predictions = []
        gray_image = cv2.cvtColor(bgr_frame, cv2.COLOR_BGR2GRAY)

        for face_rectangle in detected_faces:
            x1, x2, y1, y2 = self.__apply_offsets(face_rectangle, self.emotion_offsets)
            gray_face = gray_image[y1:y2, x1:x2]

            try:
                gray_face = cv2.resize(gray_face, self.emotion_target_size)
            except:
                return []

            gray_face = self.__preprocess_input(gray_face, True)
            gray_face = np.expand_dims(gray_face, 0)
            gray_face = np.expand_dims(gray_face, -1)
            preds = self.emotion_classifier.predict(gray_face)[0].tolist()
            predictions.append((list(face_rectangle), preds))

        return predictions
