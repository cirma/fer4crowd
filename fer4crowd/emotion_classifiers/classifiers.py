from fer4crowd.emotion_classifiers.face_api.FaceAPI import FaceAPI
from fer4crowd.emotion_classifiers.git_shanks.GitShanks import GitShanks
from fer4crowd.emotion_classifiers.little_vgg.LittleVGG import LittleVGG
from fer4crowd.emotion_classifiers.mini_xception.MiniXCEPTION import MiniXCEPTION


def get_available_classifiers():
    return {
        #"FaceAPI": FaceAPI,
        "MiniXCEPTION": MiniXCEPTION,
        "GitShanks": GitShanks,
        "LittleVGG": LittleVGG,
    }
