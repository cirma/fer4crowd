import io
from pathlib import Path
from fer4crowd.emotion_classifiers.BaseClassifier import BaseClassifier
from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials
import configparser
import cv2


class FaceAPI(BaseClassifier):

    def __init__(self):
        config = configparser.ConfigParser()
        config.read(str(Path(__file__).parent/"config.ini"))
        self.face_client = FaceClient(config['Credentials']['endpoint'],
                                      CognitiveServicesCredentials(config['Credentials']['key']))

    def get_emotion_labels(self):
        """
        :return: List of emotion labels
        """
        return ["anger", "contempt", "disgust", "fear", "happiness", "neutral", "sadness", "surprise"]

    def predict(self, bgr_frame):
        """
        :param bgr_frame: BGR frame
        :return: Emotion predictions: list of tuples (detected_face_rectangle, face_emotions)
        face_rectangle: (x,y,width,height)
        face_emotions: list of emotion predictions. It must follow the same order as the list of emotion labels returned by get_emotion_labels() method
        """
        success, buf = cv2.imencode(".jpg", bgr_frame)
        stream = io.BytesIO(buf)
        detected_faces = self.face_client.face.detect_with_stream(stream, return_face_attributes=['emotion'])
        predictions = []

        for face in detected_faces:
            rectangle = (face.face_rectangle.left, face.face_rectangle.top,
                               face.face_rectangle.width, face.face_rectangle.height)
            emotions = vars(face.face_attributes.emotion)
            emotions.pop("additional_properties")
            predictions.append((rectangle, [v for v in emotions.values()]))

        return predictions
