import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from fer4crowd.ui.gui import GUI


def main():
    GUI().run()


if __name__ == "__main__":
    main()