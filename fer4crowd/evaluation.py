import glob
import os
from pathlib import Path
from random import randint
from natsort import natsorted
import cv2
import gspread
import imutils
import numpy as np
from fer4crowd.emotion_classifiers.classifiers import get_available_classifiers
from fer4crowd.report.gspreadsheet import GoogleSheet
from fer4crowd.emotion_classifiers.BaseClassifier import BaseClassifier

GEMEP = {1: "HIGH negative",
         2: "LOW negative",
         3: "LOW positive",
         4: "HIGH positive"}


def _emo_value_to_category(value):
    """
    :param value: float
    :return:
    """
    if value < 0.25:
        return "Minimum"
    if 0.25 <= value < 0.5:
        return "Low"
    if 0.5 <= value < 0.75:
        return "Medium"
    return "High"


def _prediction_to_gemep(prediction, classifier, GEMEP_mapping):
    """
    :type prediction: list
    :type classifier: BaseClassifier
    :type GEMEP_mapping: dict
    :return: string
    """
    weighted_gemep = 0

    for i, emo in enumerate(classifier.get_emotion_labels()):
        weighted_gemep += (GEMEP_mapping[emo] * prediction[i])

    weighted_gemep /= sum(prediction)

    return GEMEP[int(np.round(weighted_gemep))]


def generate_evaluation_module(sample_frames_folder,
                               classifier_names,
                               drive_folder_id,
                               GEMEP_mapping,
                               weighted_gemep=False,
                               empty_model_id=None,
                               frame_width=-1):
    """
    :param empty_model_id:
    :param frame_width:
    :param sample_frames_folder:
    :param classifier_names: list of classifier classes to evaluate
    :type classifier_names: list of classifier names
    :param GEMEP_mapping: e.g. {"happiness":4, "neutral":3, "sadness":1}
    1 === Low (intensity) negative emotion
    2 === High (intensity) negative emotion
    3 === Low (intensity) positive emotion
    4 === High (intensity) positive emotion
    :param weighted_gemep: If True the GEMEP category will be determined by weighting emo GEMEP categories on the emo
    intensities, otherwise it will be determined taking into account the resulting best emotion.
    :type GEMEP_mapping: dict
    :param drive_folder_id:
    :return:
    """

    sheet = GoogleSheet("EvaluationModule", drive_folder_id,
                        sheet_type=GoogleSheet.TYPE_EVALUATION,
                        empty_model_id=empty_model_id)
    print("Start...")
    emos = []
    available_classif = get_available_classifiers()
    classifiers = []    # type: List[List[BaseClassifier, GoogleSheet.WorksheetWrapper or None, List]]

    for classifier_name in classifier_names:
        c = available_classif[classifier_name]()
        classifiers.append([c, None, []])
        emos += c.get_emotion_labels()

    emos = sorted(list(set(emos)))
    ws_annotator = sheet.load_worksheet("Annotator #1")
    ws_annotator.add_rows([emos])
    ws_annotator.push_update("E1")
    ws_settings = sheet.load_worksheet("Settings")
    ws_settings.add_rows([["Annotator #2"]])

    frame_imgs = natsorted(glob.glob(str(Path(sample_frames_folder) / "*")))

    for i, frame_img in enumerate(frame_imgs):
        frame_name = Path(frame_img).name
        frame = cv2.imread(frame_img)
        if frame_width > 0:
            frame = imutils.resize(frame, width=frame_width)

        ws_annotator.add_rows([[frame_name]])

        for j, classifier_name in enumerate(classifier_names):
            if i == 0:
                w = ws_annotator.ws.duplicate(insert_sheet_index=5+j, new_sheet_name=classifier_name)   # type: gspread.Worksheet
                ws_settings.add_rows([[classifier_name]])
                classifiers[j][1] = sheet.load_worksheet(w.title)   # type: GoogleSheet.WorksheetWrapper

                # indexes of the classifier emo labels in the overall emos list
                classifiers[j][2] = [emos.index(emo) for emo in classifiers[j][0].get_emotion_labels()]

            classifier, ws, emo_indexes = classifiers[j]
            predictions = classifier.predict(frame)

            if len(predictions) > 0:    # the classifier detected some faces
                overall_prediction = np.zeros(len(classifier.get_emotion_labels()))

                for prediction in predictions:
                    overall_prediction += np.array(prediction[1])

                overall_prediction /= len(predictions)
                overall_prediction = list(overall_prediction)
                row = [frame_name, len(predictions)]

                if weighted_gemep:
                    gemep_value = _prediction_to_gemep(overall_prediction, classifier, GEMEP_mapping)
                else:
                    best_emo = classifier.get_emotion_labels()[overall_prediction.index(max(overall_prediction))]
                    gemep_value = GEMEP[GEMEP_mapping[best_emo]]

                row += [gemep_value, ""]
                row_emo = [""] * len(emos)

                for k, emo_idx in enumerate(emo_indexes):
                    row_emo[emo_idx] = _emo_value_to_category(overall_prediction[k])

                row += row_emo

            else:   # no face detected
                row = [frame_name, 0] + [""] * (len(emos) + 2)   # empty row: only frame reference and number of faces (0)

            ws.add_rows([row])

            if i == len(frame_imgs)-1:  # end of frame for
                ws.push_update("A2")

    ws_settings.push_update("A3")
    ws_annotator.push_update("A2")
    ws_annotator.ws.duplicate(insert_sheet_index=5, new_sheet_name="Annotator #2")


def sample_frames(video_path, dest_folder, frame_num, time_ranges, first_progressive_number=1, frame_width=-1):
    if not os.path.exists(video_path):
        if not os.path.exists(video_path):
            raise FileNotFoundError("Video not found")

    if not os.path.exists(dest_folder):
        os.mkdir(dest_folder)

    if frame_num < len(time_ranges):
        raise Exception("The number of frames must be greater or equal to the number of ranges")

    stream = cv2.VideoCapture(video_path)
    video_fps = stream.get(cv2.CAP_PROP_FPS)
    frames_per_range = int(frame_num / len(time_ranges))
    extra_frames = frame_num % len(time_ranges)     # leftover frames
    frame_counter = first_progressive_number

    for i, time_range in enumerate(sorted(time_ranges)):
        start_time = time_range[0]
        end_time = time_range[1]
        frames = frames_per_range if i < len(time_ranges) - 1 else frames_per_range + extra_frames  # add leftover_frames to the last frames
        range_width = end_time - start_time + 1
        range_frames = int(video_fps * range_width)
        hop = int(range_frames / frames)
        next_sampling_range = int(video_fps * start_time)
        next_frame = next_sampling_range + randint(0, hop)

        for j in range(frames):
            stream.set(cv2.CAP_PROP_POS_FRAMES, next_frame)
            _, frame = stream.read()
            if frame_width > 0:
                frame = imutils.resize(frame, width=frame_width)
            file = str(Path(("%s" % dest_folder)) / ("frame_%s.png" % frame_counter))
            frame_counter += 1
            cv2.imwrite(file, frame)
            next_sampling_range += hop
            next_frame = next_sampling_range + randint(0, hop)
