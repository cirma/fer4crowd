from imutils.video import FileVideoStream
import numpy as np
import imutils
from cv2 import cv2
import os.path
import time
from statistics import variance
import os
from fer4crowd.report.gspreadsheet import GoogleSheet
from fer4crowd.report.xlsxspreadsheet import XlsxSheet


class VideoAnalyzer:
    GOOGLE_SHEET_REPORT = 1
    OFFLINE_REPORT = 2

    __killed = False

    def __init__(self, emo_classifier, ms_hop=1000, frame_width=-1, emos_to_ignore=None, progress_callback=None):
        """
        :param emo_classifier:
        :type emo_classifier: fer4crowd.emotion_classifiers.BaseClassifier.BaseClassifier
        :param ms_hop: Analyze a frame every ms_hop milliseconds
        :type ms_hop: int
        :param frame_width: Resize every frame for a faster analysis (pixels)
        :type frame_width: int
        :param emos_to_ignore: list of emotions to treat as additional properties
        :param progress_callback: callback accepting the percentage of progress (int),
        an optional next step description (string or None) and a flag (bool) telling if the process completed
        :type progress_callback: callable[int, string, bool], callable[int, None, bool]
        """

        self.progress_callback = progress_callback
        self.progress_percentage = 0
        self.ms_hop = ms_hop
        self.frame_width = frame_width
        self.classifier = emo_classifier
        self.emo_labels = self.classifier.get_emotion_labels()
        self.ignored_emos_idx = []
        ignored_emos = []

        if emos_to_ignore is not None:
            for emo in emos_to_ignore:
                if emo in self.emo_labels:
                    idx = self.emo_labels.index(emo)
                    ignored_emos.append(self.emo_labels.pop(idx))
                    self.ignored_emos_idx.append(idx)

        self.emo_num = len(self.emo_labels)
        self.additional_prop_labels = \
            [emo.upper() for emo in ignored_emos] + \
            ["#FACES"] + \
            [emo + "_VAR" for emo in self.emo_labels]

    @staticmethod
    def __frame_to_time_label(fps, frame_num, time_offset=0, milliseconds=False):
        """
        :param fps:
        :param frame_num:
        :param time_offset:
        :param milliseconds: bool
        :return: string
        Time in format mm:ss
        """
        _time = frame_num / fps + time_offset
        seconds = int(_time)
        ms = ""
        if milliseconds:
            ms = ".%03d" % ((_time - seconds) * 1000)
        minutes = int(seconds / 60)
        seconds = seconds % 60
        min_sec = "%02d:%02d" % (minutes, seconds)
        return min_sec + ms

    def __notify_progress_increment(self, percentage_increment, next_step_description=None, complete=False):
        if self.progress_callback is not None:
            self.progress_percentage += percentage_increment
            if self.progress_percentage > 100:
                self.progress_percentage = 100
            self.progress_callback(int(self.progress_percentage), next_step_description, complete)

    def analyze_video(self, video_path, report_name, time_frame=(None, None, False), time_offset=0,
                      show_milliseconds=False, visual_analysis=False, report=OFFLINE_REPORT,
                      report_folder=None, drive_folder_id=None):
        """
        :param report:
        :param video_path:
        :param report_name:
        :param time_frame: Analyze the video from second time_frame[0] to second time_frame[1].
        If time_frame[2] is True start the time labeling from time 0 (00:00).
        Set to None to ignore only time_frame[0] or time_frame[1] e.g. (None, 23, True) or (43, None, True)
        :type time_frame: (int, int, bool), (None, int, bool), (None, None, bool)
        :param time_offset: Shift the timeline labels time_offset seconds forward
        :param show_milliseconds: Show milliseconds in the report
        :param visual_analysis:
        :param report_folder: Ignored if report param is not set to OFFLINE_REPORT
        :param drive_folder_id: Desired Google D    rive report folder id. Read it from the url.
        This param will be ignored if report param is not set to GOOGLE_SHEET_REPORT.
        :type drive_folder_id: string or None
        """

        self.__killed = False

        if not os.path.exists(video_path):
            raise FileNotFoundError("Video not found")

        fvs = FileVideoStream(video_path).start()  # process frames and put them in a queue (in a separate thread)
        video_fps = fvs.stream.get(cv2.CAP_PROP_FPS)
        frame_num = fvs.stream.get(cv2.CAP_PROP_FRAME_COUNT)
        frame_hop = int(video_fps * (self.ms_hop / 1000))
        time.sleep(1.0)
        frame_count = 0  # number of read frames
        analyzed_frames = 0

        # PROGRESS INITIALIZATION
        if self.__killed:
            return
        self.__notify_progress_increment(0, "Report initialization...")

        # REPORT SHEET INITIALIZATION STEP (tot: 30% progress)
        gs_report = (report == VideoAnalyzer.GOOGLE_SHEET_REPORT)
        offline_report = (report == VideoAnalyzer.OFFLINE_REPORT)

        if gs_report:
            if drive_folder_id is None:
                raise Exception("Invalid Drive folder id.")
            sheet = GoogleSheet(report_name, drive_folder_id)
        elif offline_report:
            sheet = XlsxSheet(report_name, report_folder)
        else:
            raise Exception("Invalid report type.")

        # Init Data worksheet
        ws_data = sheet.load_worksheet("Data")
        ws_data_cols = 1 + self.emo_num
        ws_data_rows = 1
        ws_data.add_rows([["Time"] + self.emo_labels])
        self.__notify_progress_increment(4)

        # Init Other worksheet
        ws_other = sheet.load_worksheet("Other")
        ws_other_cols = 2 + len(self.ignored_emos_idx)
        ws_other.add_rows([["Time", "UNIT ID"] + self.additional_prop_labels[:len(self.ignored_emos_idx)]])
        self.__notify_progress_increment(4)

        # Init Variance Worksheet
        ws_variance = sheet.load_worksheet("Variance")
        ws_variance_cols = 2 + self.emo_num
        ws_variance.add_rows([["Time"] + self.additional_prop_labels[len(self.ignored_emos_idx):]])
        self.__notify_progress_increment(4)

        # Init Peaks Worksheet
        ws_peaks = sheet.load_worksheet("Peaks")
        ws_peaks_cols = 1 + self.emo_num
        ws_peaks.add_rows([["Time"] + [emo + "_PEAK" for emo in self.emo_labels]])
        self.__notify_progress_increment(4)

        # Init Valence Worksheet
        ws_valence = sheet.load_worksheet("Valence")
        ws_valence_cols = 10
        self.__notify_progress_increment(4, "Analyzing video...")  # PROGRESS 20%
        # END SHEET INIT

        if self.__killed:
            return

        frame_range = [1, frame_num]

        if time_frame[0] is not None:
            frame_range[0] = video_fps * time_frame[0]  # first frame to analyze
            if time_frame[2]:
                time_offset -= time_frame[0]  # start the time labeling from 00:00
        if time_frame[1] is not None:
            frame_range[1] = video_fps * time_frame[1]  # last frame to analyze

        # loop over frames from the video file stream
        while frame_count <= frame_range[1] and fvs.more() and not self.__killed:
            frame_count += 1
            self.__notify_progress_increment(50/frame_range[1], "{0}/{1} analyzed frames".format(frame_count, int(frame_range[1])))

            frame = fvs.read()  # read the next frame from the ready-frames queue

            if frame_count < frame_range[0]:  # skip initial frames up to the first to analyze
                continue  # skip this frame

            if (frame_count + frame_hop - 1) % frame_hop != 0:  # analyze 1 frame per frame_hop
                continue  # skip this frame

            if self.frame_width > 0:
                frame = imutils.resize(frame, width=self.frame_width)

            predictions = self.classifier.predict(frame)
            time_label = self.__frame_to_time_label(video_fps, frame_count, time_offset=time_offset,
                                                    milliseconds=show_milliseconds)
            detected_face_num = len(predictions)

            if detected_face_num == 0:  # no face detected
                ws_data.add_rows([[time_label] + [0] * self.emo_num])
                ws_other.add_rows([[time_label, ""] + [0] * len(self.ignored_emos_idx)])
                ws_variance.add_rows([[time_label] + [0] * (1 + self.emo_num)])
                ws_peaks.add_rows([[time_label] + [None] * (ws_peaks_cols - 1)])
                ws_data_rows += 1
                continue

            analyzed_frames += 1

            if not isinstance(predictions[0][1], list):
                raise Exception("The chosen classifier didn't return a list of tuples (list, list)")

            analyzed_faces = 0
            overall_frame_emos = np.zeros(self.emo_num, dtype="float32")
            additional_props = np.zeros(len(self.additional_prop_labels), dtype="float32")
            preds = []

            for i, prediction in enumerate(predictions):  # foreach analyzed face

                # Move the ignored emotions predictions in additional properties
                for j, idx in enumerate(self.ignored_emos_idx):
                    additional_props[j] += prediction[1].pop(idx)

                overall_frame_emos += prediction[1]
                preds.append(prediction[1])

                if visual_analysis:
                    best_label = self.emo_labels[prediction[1].index(max(prediction[1]))]
                    x, y, w, h = prediction[0]

                    # draw the face box
                    cv2.rectangle(frame, pt1=(x, y), pt2=(x + w, y + h), color=(255, 0, 0), thickness=1)

                    # show the face number
                    cv2.putText(frame, "{}".format(i + 1), (x - 10, y + h + 15),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)

                    # show the best label
                    cv2.putText(frame, best_label, (x - 10, y - 10),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 255), 2)

                analyzed_faces += 1

            overall_frame_emos /= detected_face_num  # average emo values on detected face number

            for i in range(len(self.ignored_emos_idx)):
                additional_props[i] /= detected_face_num  # average ignored emo values on detected face number

            additional_props[len(self.ignored_emos_idx)] = detected_face_num

            if detected_face_num > 1:
                # Compute variance between faces
                preds = np.array(preds, dtype="float64")
                for i in range(self.emo_num):
                    additional_props[len(self.ignored_emos_idx) + 1 + i] = variance(preds[:, i])

            ws_data.add_rows([[time_label] + overall_frame_emos.tolist()])
            ws_other.add_rows([[time_label, ""] + additional_props[0:len(self.ignored_emos_idx)].tolist()])
            ws_variance.add_rows([[time_label] + additional_props[len(self.ignored_emos_idx):].tolist()])
            ws_peaks.add_rows([[time_label] + [None] * (ws_peaks_cols - 1)])
            ws_data_rows += 1

            if visual_analysis:
                # display the frame number
                cv2.putText(frame, "Frame #{}".format(frame_count),
                            (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 1)
                # display the time
                cv2.putText(frame, time_label,
                            (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 1)

                # show the frame and update the FPS counter
                cv2.imshow("Emo analysis", frame)
                k = cv2.waitKey(1)  # show frame 1ms

                #if k == 27:  # ESC key
                    #cv2.destroyAllWindows()
                    #visual_analysis = False

        # cleanup
        if visual_analysis:
            cv2.destroyAllWindows()

        fvs.stop()

        if self.__killed:
            return

        self.__notify_progress_increment(0, "Report generation...")  # PROGRESS 70%

        # SHEET DATA UPDATE
        ws_data.push_update()
        ws_data.resize(ws_data_rows, ws_data_cols)

        self.__notify_progress_increment(3)

        # Stats worksheet
        ws_stats = sheet.load_worksheet("Stats")
        means_row = ["mean"]
        variances_row = ["variance"]
        for col_idx in range(2, self.emo_num + 2):
            col_name = GoogleSheet.col_to_name(col_idx)
            means_row.append("=AVERAGE({0}!{1}:{1})".format(ws_data.title, col_name))
            variances_row.append("=VAR({0}!{1}:{1})".format(ws_data.title, col_name))
        ws_stats.add_rows([[""] + self.emo_labels, means_row, variances_row])
        ws_stats.push_update()
        ws_stats.resize(5, self.emo_num + 1)

        self.__notify_progress_increment(3)

        ws_other.push_update()
        ws_other.resize(ws_data_rows, ws_other_cols)

        ws_variance.push_update()
        ws_variance.resize(ws_data_rows, ws_variance_cols)

        self.__notify_progress_increment(3)

        if offline_report:
            self.__notify_progress_increment(10)
            sheet.generate()
            self.__notify_progress_increment(10, complete=True)
            # END OFFLINE REPORT GENERATION

        if gs_report:
            # Peaks
            ws_peaks.push_update()

            for row_idx in range(2, ws_data_rows + 1):
                row = []
                for col_idx in range(2, ws_peaks_cols + 1):
                    col_name = GoogleSheet.col_to_name(col_idx)
                    row.append("=IF(ABS(Data!{0}{1}-Stats!{0}$2) > Stats!{0}$5; "
                               "SIGN(Data!{0}{1}-Stats!{0}$2) * (ABS(Data!{0}{1}-Stats!{0}$2) - Stats!{0}$5); 0)"
                               .format(col_name, row_idx))
                ws_peaks.add_rows([row])

            ws_peaks.push_update(range_name="B2")
            ws_peaks.resize(ws_data_rows, ws_peaks_cols)

            self.__notify_progress_increment(3)

            # Valence
            ws_valence.resize(ws_data_rows, ws_valence_cols)
            last_col_name = GoogleSheet.col_to_name(ws_data_cols)
            for row_idx in range(2, ws_data_rows + 1):
                ws_valence.add_rows([
                    ["=SUM(IFERROR(FILTER(Data!A{0}:{1}{0}; NOT(ISERROR(VLOOKUP(Data!$A$1:{1}$1; D:D; 1; FALSE))));0))".
                         format(row_idx, last_col_name)] +
                    ["=SUM(IFERROR(FILTER(Data!A{0}:{1}{0}; NOT(ISERROR(VLOOKUP(Data!$A$1:{1}$1; E:E; 1; FALSE))));0))".
                         format(row_idx, last_col_name)] +
                    ["=H{0}-I{0}".format(row_idx)]
                ])
            ws_valence.push_update(range_name="H2")

            if self.__killed:
                return
            self.__notify_progress_increment(3)

            # _Filtered worksheet
            ws_filtered = sheet.load_worksheet("_Filtered")
            ws_filtered.clear()
            ws_filtered.add_rows([
                ["=FILTER(Data!A1:{0};(Other!B:B=Unit!A1) + "
                 "(Other!B:B=\"UNIT ID\"))".format(GoogleSheet.col_to_name(ws_data_cols))] +
                [""] * (ws_data_cols - 1) +
                ["=FILTER(Variance!C1:{0};(Other!B:B=Unit!A1) + "
                 "(Other!B:B=\"UNIT ID\"))".format(GoogleSheet.col_to_name(ws_variance_cols))] +
                [""] * (ws_variance_cols - 3) +
                ["=FILTER(Peaks!B1:{0};(Other!B:B=Unit!A1) + "
                 "(Other!B:B=\"UNIT ID\"))".format(GoogleSheet.col_to_name(ws_peaks_cols))] +
                [""] * (ws_peaks_cols - 2) +
                ["=FILTER(Valence!J1:J{0};(Other!B:B=Unit!A1) + (Other!B:B=\"UNIT ID\"))".format(ws_data_rows)]
            ])
            ws_filtered.push_update()

            self.__notify_progress_increment(3)

            # Emo chart
            ws_emo_chart = sheet.load_worksheet("EmoChart")
            chart = ws_emo_chart.get_chart(0)
            chart.remove_all_domain_sources()
            chart.remove_all_series()
            chart.add_domain_source(ws_data.title, 1, 1)
            for col_idx in range(2, ws_data_cols + 1):
                chart.add_series(ws_data.title, col_idx, 1)
            chart.push_update()

            self.__notify_progress_increment(3)

            # Unit worksheet
            ws_unit = sheet.load_worksheet("Unit")

            # Unit - Emo chart
            chart = ws_unit.get_chart(1)
            chart.remove_all_domain_sources()
            chart.remove_all_series()
            chart.add_domain_source(ws_filtered.title, 1, 1, ws_data_rows)
            for col_idx in range(2, ws_data_cols + 1):
                chart.add_series(ws_filtered.title, col_idx, 1, ws_data_rows)
            chart.push_update()

            self.__notify_progress_increment(3)

            # Unit - Peak chart
            chart = ws_unit.get_chart(2)
            chart.remove_all_domain_sources()
            chart.remove_all_series()
            chart.add_domain_source(ws_filtered.title, 1, 1, ws_data_rows)
            for col_idx in range(ws_data_cols + ws_variance_cols - 1,
                                 ws_data_cols + ws_variance_cols + ws_peaks_cols - 2):
                chart.add_series(ws_filtered.title, col_idx, 1, ws_data_rows)
            chart.push_update()

            # Unit - Variance chart
            chart = ws_unit.get_chart(3)
            chart.remove_all_domain_sources()
            chart.remove_all_series()
            chart.add_domain_source(ws_filtered.title, 1, 1, ws_data_rows)
            for col_idx in range(ws_data_cols + 1, ws_data_cols + ws_variance_cols - 1):
                chart.add_series(ws_filtered.title, col_idx, 1, ws_data_rows)
            chart.push_update()

            self.__notify_progress_increment(3)

            # Unit - Valence chart
            chart = ws_unit.get_chart(0)
            chart.remove_all_domain_sources()
            chart.remove_all_series()
            chart.add_domain_source(ws_filtered.title, 1, 1, ws_data_rows)
            filtered_valence_column = ws_data_cols + ws_variance_cols + ws_peaks_cols - 2
            chart.add_series(ws_filtered.title, filtered_valence_column, 1, ws_data_rows)
            ws_unit.add_rows(
                [["=AVERAGE({0}!{1}:{1})".format(ws_filtered.title, GoogleSheet.col_to_name(filtered_valence_column))],
                 ["=STDEV({0}!{1}:{1})".format(ws_filtered.title, GoogleSheet.col_to_name(filtered_valence_column))]])
            ws_unit.push_update(range_name="O2")
            chart.push_update()

            self.__notify_progress_increment(3, complete=True)

            # END DRIVE REPORT GENERATION

    def stop(self):
        self.__killed = True
