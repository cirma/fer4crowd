from pathlib import Path
import pandas as pd
from xlsxwriter.utility import xl_cell_to_rowcol


class XlsxSheet:

    class WorksheetWrapper:
        def __init__(self, title, writer):
            self.df = pd.DataFrame()
            self.title = title
            self.writer = writer

        def add_rows(self, rows):
            """
            Add rows in the worksheet.
            The update will be in the staging area until push_update() or clear_staging_area() are executed.
            :type rows: list of list
            """
            self.df = self.df.append(pd.DataFrame(rows))

        def resize(self, rows, cols):
            pass

        def clear(self):
            pass

        def clear_staging_area(self):
            self.df = pd.DataFrame()

        def push_update(self, range_name=None):
            row, col = (0, 0)
            if range_name is not None:
                row, col = xl_cell_to_rowcol(range_name)
            self.df.to_excel(self.writer, sheet_name=self.title, startrow=row, startcol=col, header=False, index=False)
            self.clear_staging_area()

    def __init__(self, sheet_name, folder):
        self.writer = pd.ExcelWriter(str(Path(folder)/(sheet_name+".xlsx")), engine="xlsxwriter",
                                     options={'strings_to_formulas': True})
        self.ws = {}

    def load_worksheet(self, worksheet_title):
        self.ws[worksheet_title] = self.WorksheetWrapper(worksheet_title, self.writer)
        return self.get_ws(worksheet_title)

    def get_ws(self, worksheet_title):
        return self.ws[worksheet_title]

    def generate(self):
        self.writer.close()

