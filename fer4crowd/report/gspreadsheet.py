import os
import socket
from pathlib import Path

import gspread
import pandas as pd
from googleapiclient import discovery
from xlsxwriter.utility import xl_col_to_name
import json


class GoogleSheet:
    SPREADSHEET_URL = 'https://sheets.googleapis.com/v4/spreadsheets'
    SPREADSHEET_GET_CHARTS = "/%s?fields=sheets(charts)"
    SPREADSHEET_BATCH_UPDATE = "/%s:batchUpdate"
    API_CREDENTIALS_PATH = str(Path(__file__).parents[1] / "config" / "google_api_credentials.json")
    DRIVE_FOLDER_BASE_URL = "https://drive.google.com/drive/folders/%s"
    REPORT_MODEL_FILE = str(Path(__file__).parent / "gspreadsheet_model.json")
    EVALUATION_MODEL_FILE = str(Path(__file__).parent / "gspreadsheet_EVAL_model.json")
    TYPE_REPORT = 1
    TYPE_EVALUATION = 2

    @staticmethod
    def col_to_name(col_index):
        return xl_col_to_name(col_index - 1)

    @staticmethod
    def generate_model(source_sheet_name, sheet_type=TYPE_REPORT):
        gc = gspread.service_account(filename=GoogleSheet.API_CREDENTIALS_PATH)
        sh = gc.open(source_sheet_name)
        model = gc.request(
            "get",
            "https://sheets.googleapis.com/v4/spreadsheets/{0}?fields=*&includeGridData=true".format(sh.id)).json()
        del model["spreadsheetId"]
        model_file = GoogleSheet.REPORT_MODEL_FILE if sheet_type == GoogleSheet.TYPE_REPORT \
            else GoogleSheet.EVALUATION_MODEL_FILE
        with open(model_file, "w+") as mf:
            json.dump(model, mf)

    class WorksheetWrapper:
        def __init__(self, worksheet, spreadsheet):
            """
            :type worksheet: gspread.models.Worksheet
            """
            self.ws = worksheet
            self.df = pd.DataFrame()
            self.spreadsheet = spreadsheet
            self.charts = None
            self.title = self.ws.title

        def add_rows(self, rows):
            """
            Add rows in the worksheet.
            The update will be in the staging area until push_update() or clear_staging_area() are executed.
            :type rows: list of list
            """
            self.df = self.df.append(pd.DataFrame(rows))

        def resize(self, rows, cols):
            self.ws.resize(rows, cols)

        def clear(self):
            self.ws.clear()

        def clear_staging_area(self):
            self.df = pd.DataFrame()

        def push_update(self, range_name=None):
            if range_name is not None:
                self.ws.update(range_name, self.df.values.tolist(), raw=False)
            else:
                self.ws.update(self.df.values.tolist(), raw=False)
            self.clear_staging_area()

        def get_chart(self, chart_idx):
            if self.charts is None:
                resp = self.spreadsheet.sh.client.request(
                    "get", GoogleSheet.SPREADSHEET_URL + GoogleSheet.SPREADSHEET_GET_CHARTS % self.spreadsheet.sh.id)
                self.charts = resp.json()["sheets"][self.ws._properties["index"]]["charts"]
            chart = self.charts[chart_idx]
            del chart["position"]
            return GoogleSheet.Chart(chart, self.spreadsheet)

    class Chart:
        def __init__(self, json_definition, spreadsheet):
            self.chart = json_definition
            self.spreadsheet = spreadsheet

        def remove_all_domain_sources(self):
            self.chart["spec"]["basicChart"]["domains"] = []

        def add_domain_source(self, src_worksheet_title,  column_id, start_row_id, end_row_id=None):
            source = {
                "sheetId": self.spreadsheet.sh.worksheet(src_worksheet_title).id,
                "startRowIndex": start_row_id - 1,  # inclusive
                "startColumnIndex": column_id - 1,  # inclusive
                "endColumnIndex": column_id     # exclusive
            }

            if end_row_id is not None:
                source["endRowIndex"] = end_row_id  # exclusive

            self.chart["spec"]["basicChart"]["domains"].append({
                "domain": {
                    "sourceRange": {
                        "sources": [source]
                    }
                }
            })

        def remove_all_series(self):
            self.chart["spec"]["basicChart"]["series"] = []

        def add_series(self, src_worksheet_title,  column_id, start_row_id, end_row_id=None):
            source = {
                "sheetId": self.spreadsheet.sh.worksheet(src_worksheet_title).id,
                "startRowIndex": start_row_id - 1,  # inclusive
                "startColumnIndex": column_id - 1,  # inclusive
                "endColumnIndex": column_id     # exclusive
            }

            if end_row_id is not None:
                source["endRowIndex"] = end_row_id  # exclusive

            self.chart["spec"]["basicChart"]["series"].append({
                "series": {
                    "sourceRange": {
                        "sources": [source]
                    }
                }
            })

        def push_update(self):
            body = {'requests': [{'updateChartSpec': self.chart}]}
            self.spreadsheet.sh.client.request(
                'post', GoogleSheet.SPREADSHEET_URL + GoogleSheet.SPREADSHEET_BATCH_UPDATE % self.spreadsheet.sh.id,
                json=body
            )

    def __init__(self, sheet_name, drive_folder_id, sheet_type=TYPE_REPORT, empty_model_id=None):
        """
        :param sheet_name:
        :param drive_folder_id: Desired Google Drive report folder id. Read it from the url.
        :type drive_folder_id: string
        :param sheet_type: GoogleSpreadsheet.TYPE_REPORT or GoogleSpreadsheet.TYPE_EVALUATION
        """
        self.gc = gspread.service_account(filename=GoogleSheet.API_CREDENTIALS_PATH)
        self.ws = {}

        if empty_model_id is None:
            self.sh = self.__create_sheet_from_model(sheet_name, drive_folder_id, sheet_type)
        else:
            self.sh = self.gc.open_by_key(empty_model_id)

    def __create_sheet_from_model(self, sheet_name, drive_folder_id, sheet_type):
        model_file = GoogleSheet.REPORT_MODEL_FILE if sheet_type == GoogleSheet.TYPE_REPORT \
            else GoogleSheet.EVALUATION_MODEL_FILE

        if not os.path.exists(model_file):
            raise Exception("No model found. Generate one with GoogleSheet.generate_model() method.")
        with open(model_file) as mf:
            model = json.load(mf)

        model["properties"]["title"] = sheet_name

        done = False
        while not done:
            done = True
            try:
                sheet_service = discovery.build('sheets', 'v4', credentials=self.gc.auth)
                request = sheet_service.spreadsheets().create(body=model)
                response = request.execute()
                # The new sheet was created in the Service Account's Drive root folder.
                # Let's move it in the desired folder.
                drive_service = discovery.build('drive', 'v3', credentials=self.gc.auth)
                file_id = response["spreadsheetId"]
                old_folder_id = drive_service.files().get(fileId=file_id, fields="parents").execute()['parents'][0]
                drive_service.files().update(fileId=file_id, addParents=drive_folder_id, removeParents=old_folder_id,
                                              fields="id,parents").execute()
            except socket.timeout:
                print("Google API timeout. Retrying...")
                done = False

        return self.gc.open_by_key(file_id)

    def load_worksheet(self, worksheet_title):
        """
        :rtype: GoogleSheet.WorksheetWrapper
        """
        self.ws[worksheet_title] = self.WorksheetWrapper(self.sh.worksheet(worksheet_title), self)
        return self.get_ws(worksheet_title)

    def get_ws(self, worksheet_title):
        """
        :rtype: GoogleSheet.WorksheetWrapper
        """
        return self.ws[worksheet_title]